all:
	mkdir -p build
	pandoc bylaws.md -o build/bylaws.pdf

clean:
	rm build/bylaws.pdf
