# ARTICLE I. NAME OF ORGANIZATION

The name of the organization shall be Quaternion Institute. It shall be a nonprofit organization incorporated under the laws of the State of Washington.

# ARTICLE II. CORPORATE PURPOSE

## Section 1. Nonprofit Purpose 

This corporation is organized exclusively for charitable, religious, educational, and scientific purposes, including, for such purposes, the making of 
distributions to organizations that qualify as exempt organizations under section 501(c)(3) of the Internal Revenue Code, or the corresponding section of 
any future federal tax code.

## Section 2. Specific Purpose 

To create an innovative and accessible free online educational ecosystem that utilizes modern technology and applies advancements from research in 
education.

# ARTICLE III. MEMBERSHIP 

The membership of the corporation shall consist of the members of the Board of Directors.

# ARTICLE IV. BOARD OF DIRECTORS 

## Section 1. General Powers

The affairs of the Corporation shall be managed by its Board of Directors. The Board of Directors shall have control of and be responsible for the 
management of the affairs and property of the Corporation.

## Section 2. Number, Tenure, Requirements, and Qualifications

a. The number of Directors shall be fixed from time-to-time by the Directors but shall consist of no less than two (2) nor more than eleven (11) 
including the following officers: the President, the Vice-President, the Secretary, and the Treasurer.

b. The members of the Board of Directors shall, upon election, immediately enter upon the performance of their duties and shall continue in office until 
their successors shall be duly elected and qualified. All members of the Board of Directors and Advisory Council must be approved by a majority vote of 
the members present and voting. No vote on new members of the Board of Directors, or Advisory Council, shall be held unless a quorum of the Board of 
Directors is present as provided in Section 6 of this Article.

c. No two members of the Board of Directors related by blood or marriage/domestic partnership within the second degree of consanguinity or affinity may 
serve on the Board of Directors at the same time.

d. Each elected member shall serve an indefinite term, not withstanding resignation, death, or a vote of no confidence as elucidated in Section 12.

e. Each member of the board of directors shall attend a minimum of 10 monthly meetings per annum, unless a majority of the board votes to excuse excess
absences for reasonable preoccupations or emergencies.

## Section 3. Regular and Annual Meetings

An annual meeting of the Board of Directors shall be held at a time and day in the month of September of each calendar year and at a location designated 
by the Executive Committee of the Board of Directors. The Board of Directors may provide by resolution the time, for the holding of regular meetings of 
the Board. Notice of these meetings shall be sent to all members of the Board of Directors no less than ten (10) days, prior to the meeting 
date and shall exclusively take place in a secure digital chat platform.

## Section 4. Special Meetings

Special meetings of the Board of Directors may be called by or at the request of the president or at least two members of the Executive Committee. The 
person(s) authorized to call special meetings of the Board of Directors may only fix a secure digital chat platform, as the place for holding any special 
meeting of the Board called by them.

## Section 5. Notice

Notice of any special meeting of the Board of Directors shall be given at least two (2) days in advance of the meeting by telephone or email. Any 
Director may waive notice of any meeting. The attendance of a Director at any meeting shall constitute a waiver of notice of such meeting, except where a 
Director attends a meeting for the express purpose of objecting to the transaction of any business because the meeting is not lawfully called or 
convened. Neither the business to be transacted at, nor the purpose of, any regular meeting of the Board of Directors need be specified in the notice or 
waiver of notice of such meeting, unless specifically required by law or by these by-laws.

## Section 6. Quorum

The presence, of a majority of current members of the Board of Directors shall be necessary at any meeting to constitute a quorum to transact business. 
The act of a majority of the members of the Board of Directors present at a meeting at which a quorum is present shall be the act of the Board of 
Directors, unless the act of a greater number is required by law or by these by-laws.

## Section 7. Forfeiture

Any member of the Board of Directors who fails to fulfill any of their requirements as set forth in Section 2 of this Article by September 1st shall 
automatically forfeit their seat on the Board. The Secretary shall notify the Director in writing that their seat has been declared vacant, and 
the Board of Directors may forthwith immediately proceed to fill the vacancy. Members of the Board of Directors who are removed for failure to meet any 
or all of the requirements of Section 2 of this Article are not entitled to vote at the annual meeting and are not entitled to the procedure outlined in 
Section 12 of this Article in these by-laws.

## Section 8. Vacancies

Whenever any vacancy occurs in the Board of Directors it shall be filled without undue delay by a majority vote of the remaining members of the Board of 
Directors at a regular meeting. Vacancies may be created and filled according to specific methods approved by the Board of Directors.

## Section 9. Compensation

Members of the Board of Directors shall not receive any compensation for their services as Directors. 

## Section 10. Confidentiality

Directors shall not discuss or disclose information about the Corporation or its activities to any person or entity unless such information is already a 
matter of public knowledge, such person or entity has a need to know, or the disclosure of such information is in furtherance of the Corporations’ 
purposes, or can reasonably be expected to benefit the Corporation. Directors shall use discretion and good business judgment in discussing the affairs 
of the Corporation with third parties. Without limiting the foregoing, Directors may discuss upcoming fundraisers and the purposes and functions of the 
Corporation, including but not limited to accounts on deposit in financial institutions. Each Director shall execute a confidentiality agreement 
consistent herewith upon being voted onto and accepting appointment to the Board of Directors.

## Section 11. Parliamentary Procedure

Any question concerning parliamentary procedure at meetings shall be determined by the President by reference to Robert’s Rules of Order.

## Section 12. Removal

Any member of the Board of Directors may be removed with cause under a vote of no confidence motion, at any time, by vote of three-quarters (3/4) of the 
members of the Board of Directors (excluding the member in question) if in their judgment the best interest of the Corporation would be served thereby. 
Each member of the Board of Directors must receive notice of the proposed removal at least five (5) days in advance of the proposed action. An officer 
who has been removed as a member of the Board of Directors shall automatically be removed from office. Members of the Board of Directors who are removed 
for failure to meet the minimum requirements in Section 2 of this Article in these by-laws automatically forfeit their positions on the Board pursuant to 
Section 7 of this Article, and are not entitled to the removal procedure outlined in Section 12 of this Article.

# ARTICLE V. OFFICERS 

The officers of this Board shall be the President, Vice-President, Secretary and Treasurer. All officers must have the status of active members of the 
Board.

## Section 1. President

The president shall preside at all meetings of the membership. The president has the following duties:

a. They shall preside at all meetings of the Executive Committee.
b. They shall have general superintendence and direction of all other officers of this corporation and see that their duties are properly performed.
c. They shall submit a report of the operations of the program for the fiscal year to members at their annual meetings, and from time to time, shall 
report to the Board all matters that may affect this program.
d. They shall be Ex-officio member of all standing committees and shall have the power and duties usually vested in the office of the President.

## Section 2. Vice-President

The Vice-President shall be vested with all the powers and shall perform all the duties of the President during the absence of the latter. The 
Vice-President has the following duties:

a. They shall have the duty of chairing their perspective committee and such other duties as may, from time to time, be determined by the Board of 
Directors. 

## Section 3. Secretary

The Secretary shall attend all meetings of the Board of Directors and of the Executive Committee, and all meetings of members, and will act as a clerk 
thereof. The Secretary has the following duties:

a. They shall record all votes and minutes of all proceedings in a book to be kept for that purpose. 
b. They, in concert with the President, shall make the arrangements for all meetings, including the annual meeting of the organization. They shall send 
notices of all meetings to the members of the Board of Directors and shall take reservations for the meetings.

## Section 4. Treasurer

The Treasurer has the following duties:

a. They shall submit for the Finance and Fund Development Committee approval of all expenditures of funds and proposed capital expenditures (equipment 
and furniture).
b. They shall present a complete and accurate report of the finances raised at each meeting of the members
c. They shall have the right of inspection of the funds utilized by any internal programs, including any budgets and subsequent audit reports. It shall 
be the duty of the Treasurer to assist in direct audits of the funds of these programs according to funding source guidelines and generally accepted 
accounting principles.
d. They shall perform such other duties as may be prescribed by the President under whose supervision they shall be.

## Section 5. Election of Officers

a. The Nominating Committee shall submit at the meeting prior to the annual meeting the names of those persons for the respective offices of the Board of 
Directors. Nominations shall also be received from the floor after the report of the Nominating Committee. The election shall be held at the annual 
meeting of the Board of Directors. Each elected member shall serve an indefinite term, not withstanding resignation, death, or a vote of no confidence as 
elucidated in Section 6.

## Section 6. Removal

The removal process for officers is identical to that of any other board member as laid out in Article IV Section 12.

## Section 7. Vacancies

The system of vacancy handling is identical to that of any other board member as laid out in Article IV Section 8.

# ARTICLE VI. COMMITTEES

## Section 1. Committee Formation

The board may create committees as needed, such as fundraising, housing, public relations, data collection, etc. The board chair appoints all committee 
chairs

## Section 2. Executive Committee

The four officers serve as the members of the Executive Committee. Except for the power to amend the Articles of Incorporation and Bylaws, the Executive 
Committee shall have all the powers and authority of the board of directors in the intervals between meetings of the board of directors, and is subject 
to the direction and control of the full board.

## Section 3. Finance Committee

The treasurer is the chair of the Finance Committee, which includes three other board members. The Finance Committee is responsible for developing and 
reviewing fiscal procedures, fundraising plans, and the annual budget with staff and other board members. The board must approve the budget and all 
expenditures must be within budget. Any major change in the budget must be approved by the board or the Executive Committee. The fiscal year shall be the 
calendar year. Annual reports are required to be submitted to the board showing income, expenditures, and pending income. The financial records of the 
organization are public information and shall be made available to the membership, board members, and the public.

# ARTICLE VII. CORPORATE STAFF

## Section 1. Executive Director

The Board of Directors may hire an Executive Director who shall serve at the will of the Board. The Executive Director shall have immediate and overall 
supervision of the operations of the Corporation, and shall direct the day-to-day business of the Corporation, maintain the properties of the 
Corporation, hire, discharge, and determine the salaries and other compensation of all staff members under the Executive Director’s supervision, and 
perform such additional duties as may be directed by the Executive Committee or the Board of Directors. No officer, Executive Committee member or member 
of the Board of Directors may individually instruct the Executive Director or any other employee. The Executive Director shall make such reports at the 
Board and Executive Committee meetings as shall be required by the President or the Board. The Executive Director shall be an ad-hoc member of all 
committees.

The Executive Director may not be related by blood or marriage/domestic partnership within the second degree of consanguinity or affinity to any member 
of the Board of Directors or Advisory Council. The Executive Director may be hired at any meeting of the Board of Directors by a majority vote and shall 
serve until removed by the Board of Directors upon an affirmative vote of three-quarters (3/4) of the members present at any meeting of the Board 
Directors. Such removal may be with or without cause. Nothing herein shall confer any compensation or other rights on any Executive Director, who shall 
remain an employee terminable at will, as provided in this Section.

# ARTICLE VIII. CONFLICT OF INTEREST & COMPENSATION

## Section 1. Purpose

The purpose of the conflict of interest policy is to protect this tax-exempt organization’s interest when it is contemplating entering into a transaction 
or arrangement that might benefit the private interest of an officer or director of the Organization or might result in a possible excess benefit 
transaction. This policy is intended to supplement but not replace any applicable state and federal laws governing conflict of interest applicable to 
nonprofit and charitable organizations.

## Section 2. Definitions

*Interested Person*: Any director, principal officer, or member of a committee with governing board delegated powers, who has a direct or indirect 
financial interest, as defined below, is an interested person. *Financial Interest*: A person has a financial interest if the person has, directly or 
indirectly, through business, investment, or family:

a. An ownership or investment interest in any entity with which the Organization has a transaction or arrangement,
b. A compensation arrangement with the Organization or with any entity or individual with which the Organization has a transaction or arrangement, or
c. A potential ownership or investment interest in, or compensation arrangement with, any entity or individual with which the Organization is negotiating 
a transaction or arrangement.

Compensation includes direct and indirect remuneration as well as gifts or favors that are not insubstantial. A financial interest is not necessarily a 
conflict of interest; a person who has a financial interest may have a conflict of interest only if the appropriate governing board or committee decides 
that a conflict of interest exists.

## Section 3. Procedures

a. Duty to Disclose: In connection with any actual or possible conflict of interest, an interested person must disclose the existence of the financial 
interest and be given the opportunity to disclose all material facts to the directors and members of committees with governing board delegated powers 
considering the proposed transaction or arrangement.
b. Determining Whether a Conflict of Interest Exists: After disclosure of the financial interest and all material facts, and after any discussion with 
the interested person, he/she shall leave the governing board or committee meeting while the determination of a conflict of interest is discussed and 
voted upon. The remaining board or committee members shall decide if a conflict of interest exists.
c. Procedures for Addressing the Conflict of Interest
    1. An interested person may make a presentation at the governing board or committee meeting, but after the presentation, he/she shall leave the 
    meeting during the discussion of, and the vote on, the transaction or arrangement involving the possible conflict of interest.
    2. The chairperson of the governing board or committee shall, if appropriate, appoint a disinterested person or committee to investigate alternatives 
    to the proposed transaction or arrangement.
    3. After exercising due diligence, the governing board or committee shall determine whether the Organization can obtain with reasonable efforts a 
    more advantageous transaction or arrangement from a person or entity that would not give rise to a conflict of interest.
    4. If a more advantageous transaction or arrangement is not reasonably possible under circumstances not producing a conflict of interest, the 
    governing board or committee shall determine by a majority vote of the disinterested directors whether the transaction or arrangement is in the 
    organization’s best interest, for its own benefit, and whether it is fair and reasonable. In conformity with the above determination it shall make 
    its decision as to whether to enter into the transaction or arrangement.
d. Violations of the Conflict of Interest Policy
    1. If the governing board or committee has reasonable cause to believe a member has failed to disclose actual or possible conflicts of interest, it 
    shall inform the member of the basis for such belief and afford the member an opportunity to explain the alleged failure to disclose.
    2. If, after hearing the member’s response and after making further investigation as warranted by the circumstances, the governing board or committee 
    determines the member has failed to disclose an actual or possible conflict of interest, it shall take appropriate disciplinary and corrective action.

## Section 4. Records of Proceedings

The minutes of the governing board and all committees with board delegated powers shall contain:

* The names of the persons who disclosed or otherwise were found to have a financial interest in connection with an actual or possible conflict of 
interest
* The nature of the financial interest 
* Any action taken to determine whether a conflict of interest was present
* The governing board’s or committee’s decision as to whether a conflict of interest in fact existed
* The names of the persons who were present for discussions and votes relating to the transaction or arrangement
* The content of the discussion, including any alternatives to the proposed transaction or arrangement 
* A record of any votes taken in connection with the proceedings.

## Section 5. Compensation

a. A voting member of the governing board who receives compensation, directly or indirectly, from the Organization for services is precluded from voting 
on matters pertaining to that member’s compensation.
b. A voting member of any committee whose jurisdiction includes compensation matters and who receives compensation, directly or indirectly, from the 
Organization for services is precluded from voting on matters pertaining to that member’s compensation.
c. No voting member of the governing board or any committee whose jurisdiction includes compensation matters and who receives compensation, directly or 
indirectly, from the Organization, either individually or collectively, is prohibited from providing information to any committee regarding compensation.

## Section 6. Annual Statements

Each director, principal officer and member of a committee with governing board delegated powers shall annually sign a statement which affirms such 
person:

a. Has received a copy of the conflicts of interest policy,
b. Has read and understood the policy,
c. Has agreed to comply with the policy, and
d. Understands the Organization is charitable and in order to maintain its federal tax exemption it must engage primarily in activities which accomplish 
one or more of its tax-exempt purposes.

## Section 7. Periodic Reviews

To ensure the Organization operates in a manner consistent with charitable purposes and does not engage in activities that could jeopardize its tax-
exempt status, periodic reviews shall be conducted. The periodic reviews shall, at a minimum, include the following subjects:

a. Whether compensation arrangements and benefits are reasonable, based on competent survey information, and the result of arm’s length bargaining
b. Whether partnerships, joint ventures, and arrangements with management organizations conform to the Organization’s written policies, are properly 
recorded, reflect reasonable investment or payments for goods and services, further charitable purposes and do not result in inurement, impermissible 
private benefit or in an excess benefit transaction.

## Section 8. Use of Outside Experts

When conducting the periodic reviews as provided for in Article VII, the Organization may, but need not, use outside advisors. If outside experts are 
used, their use shall not relieve the governing board of its responsibility for ensuring periodic reviews are conducted.

# ARTICLE IX. INDEMNIFICATION

## Section 1. General

To the full extent authorized under the laws of the State of Washington, the corporation shall indemnify any director, officer, employee, or agent, or 
former member, director, officer, employee, or agent of the corporation, or any person who may have served at the corporation’s request as a director or 
officer of another corporation (each of the foregoing members, directors, officers, employees, agents, and persons is referred to in this Article 
individually as an “indemnitee”), against expenses actually and necessarily incurred by such indemnitee in connection with the defense of any action, 
suit, or proceeding in which that indemnitee is made a party by reason of being or having been such member, director, officer, employee, or agent, except 
in relation to matters as to which that indemnitee shall have been adjudged in such action, suit, or proceeding to be liable for negligence or misconduct 
in the performance of a duty. The foregoing indemnification shall not be deemed exclusive of any other rights to which an indemnitee may be entitled 
under any bylaw, agreement, resolution of the Board of Directors, or otherwise.

## Section 2. Expenses

Expenses (including reasonable attorneys’ fees) incurred in defending a civil or criminal action, suit, or proceeding may be paid by the corporation in 
advance of the final disposition of such action, suit, or proceeding, if authorized by the Board of Directors, upon receipt of an undertaking by or on 
behalf of the indemnitee to repay such amount if it shall ultimately be determined that such indemnitee is not entitled to be indemnified hereunder.

## Section 3. Insurance

The corporation may purchase and maintain insurance on behalf of any person who is or was a member, director, officer, employee, or agent against any 
liability asserted against such person and incurred by such person in any such capacity or arising out of such person’s status as such, whether or not 
the corporation would have the power or obligation to indemnify such person against such liability under this Article.

# ARTICLE X. BOOKS AND RECORDS

The corporation shall keep complete books and records of account and minutes of the proceedings of the Board of Directors.

# ARTICLE XI. AMENDMENTS

## Section 1. Articles of Incorporation

The Articles may be amended in any manner at any regular or special meeting of the Board of Directors, provided that specific written notice of the 
proposed amendment of the Articles setting forth the proposed amendment or a summary of the changes to be effected thereby shall be given to each 
director at least three days in advance of such a meeting if delivered by email or telephone. As required by the Articles, any amendment to Article III 
or Article V of the Articles shall require the affirmative vote of all directors then in office. All other amendments of the Articles shall require the 
affirmative vote of a simple majority of directors then in office.

## Section 2. Bylaws

The Board of Directors may amend these Bylaws by majority vote at any regular or special meeting. Written notice setting forth the proposed amendment or 
summary of the changes to be effected thereby shall be given to each director within the time and the manner provided for the giving of notice of 
meetings of directors.

# ADOPTION OF BYLAWS

We, the undersigned, are all of the initial directors or incorporators of this corporation, and we consent to, and hereby do, adopt the foregoing Bylaws, 
consisting of the 11 preceding pages, as the Bylaws of this corporation.
